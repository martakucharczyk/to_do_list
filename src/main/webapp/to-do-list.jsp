<%--
  Created by IntelliJ IDEA.
  User: 48665
  Date: 19.07.2020
  Time: 10:00
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>TO-DO list</title>
</head>
<body>
<h1>TO-DO list</h1>
<%--<% out.print("tu będą twoje taski"); %>--%>
<c:forEach var="task" items="${tasks}">
    <c:out value="${task}"/><br>
</c:forEach>
<br>
<%! String error = null; %>
<%
    error = (String) request.getAttribute("error");
    if (error != null) {
        out.print("<br>" + error);
    }
%>
<br>
<a href="add-task.jsp">Add new task to your TO-DO list</a>
<h3>Delete post</h3>
<form action="delete" method="POST">
    Task ID: <input type="text" name="id"><br>
    <input type="submit" value="Submit"/>
</form><br>
<a href="delete">Delete all tasks</a>
<br>
<br>
<br>
<%@include file="logout.jsp"%>

</body>
</html>
