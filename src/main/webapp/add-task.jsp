<%--
  Created by IntelliJ IDEA.
  User: 48665
  Date: 18.07.2020
  Time: 21:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add new task</title>
</head>
<body>
<h1>Add task</h1>
<form action="newTask" method="POST">
    Title: <input type="text" name="title"/><br>
    Content: <input type="text" name="content"/><br>
    <input type="submit" value="Submit"/>
</form>

<%! String error = null; %>
<%
    error = (String) request.getAttribute("error");
    if (error != null) {
        out.print("<br>" + error);
    }
%>
<br>
<a href="list">All your tasks </a>
<br>
<%@include file="logout.jsp"%>


</body>
</html>
