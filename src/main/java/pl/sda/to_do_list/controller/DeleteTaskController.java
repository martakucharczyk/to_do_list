package pl.sda.to_do_list.controller;

import pl.sda.to_do_list.model.Task;
import pl.sda.to_do_list.model.TaskDAO;
import pl.sda.to_do_list.model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

@WebServlet(name="delete", value = "/delete")
public class DeleteTaskController extends HttpServlet {
    private TaskDAO taskDAO;
    private RequestDispatcher dispatcher;
    private User userLogged;

    @Override
    public void init() throws ServletException {
        taskDAO = new TaskDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        userLogged = (User) req.getSession().getAttribute("userLogged");
        taskDAO.deleteAllTask(userLogged);
        Set<Task> tasks = taskDAO.findAllTaskByUserID(userLogged.getId());
        req.setAttribute("tasks", tasks);
        dispatcher = req.getRequestDispatcher("/to-do-list.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        userLogged = (User) req.getSession().getAttribute("userLogged");
        String id = req.getParameter("id");
        taskDAO.deleteTaskById(Long.parseLong(id));
        req.setAttribute("tasks", taskDAO.findAllTaskByUserID(userLogged.getId()));
        dispatcher = req.getRequestDispatcher("/to-do-list.jsp");
        dispatcher.forward(req, resp);
    }
}
