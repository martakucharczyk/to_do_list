package pl.sda.to_do_list.controller;

import com.mysql.cj.Session;
import pl.sda.to_do_list.model.User;
import pl.sda.to_do_list.model.UserDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

@WebServlet(name = "logController", value = "/log")
public class LogController extends HttpServlet {

    private UserDAO userDAO;

    @Override
    public void init() throws ServletException {
        userDAO = new UserDAO();
    }

//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/create-new-account.jsp");
//        dispatcher.forward(req, resp);
//    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        RequestDispatcher dispatcher;
        Boolean isAuthorized;

        if (login.equals("") || password.equals("")) {
            req.setAttribute("error", new String("Enter you login and password!"));
            dispatcher = getServletContext().getRequestDispatcher("/log.jsp");
        } else {
            isAuthorized = authorization(login, password);
            if (!isAuthorized) {
                req.setAttribute("error", new String("Incorrect password! Try again!"));
                dispatcher = getServletContext().getRequestDispatcher("/log.jsp");
            } else {
                req.getSession().setAttribute("userLogged", userDAO.findByLogin(login).get());
                dispatcher = getServletContext().getRequestDispatcher("/add-task.jsp");
            }
        }
        dispatcher.forward(req, resp);
    }

    private boolean authorization(String login, String password) {
        Optional<User> user = userDAO.findByLogin(login);
        if (user.isPresent()) {
            String userPassword = user.get().getPassword();
            if(userPassword.equals(password)) {
                return true;
            }
        }
        return false;
    }


}
