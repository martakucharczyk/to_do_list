package pl.sda.to_do_list.controller;

import pl.sda.to_do_list.model.Task;
import pl.sda.to_do_list.model.TaskDAO;
import pl.sda.to_do_list.model.User;
import pl.sda.to_do_list.model.UserDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name="newTask", value = "/newTask")
public class NewTaskController extends HttpServlet {

    private TaskDAO taskDAO;
    private RequestDispatcher dispatcher;

    @Override
    public void init() throws ServletException {
        taskDAO = new TaskDAO();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title = req.getParameter("title");
        String content = req.getParameter("content");
        User user = (User) req.getSession().getAttribute("userLogged");

        if(!content.trim().equals("") && !title.trim().equals("")) {
            Task task = new Task(title, content, user);
            taskDAO.create(task);
            req.setAttribute("tasks", taskDAO.findAllTaskByUserID(user.getId()));
            dispatcher = req.getRequestDispatcher("/to-do-list.jsp");
        } else {
            req.setAttribute("error", new String("Complete the data correctly"));
            dispatcher = req.getRequestDispatcher("/add-task.jsp");
        }
        dispatcher.forward(req, resp);
    }
}
