package pl.sda.to_do_list.controller;

import pl.sda.to_do_list.model.User;
import pl.sda.to_do_list.model.UserDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet(name = "create", value = "/create")
public class CreateController extends HttpServlet {

    private UserDAO userDAO;
    private RequestDispatcher dispatcher;

    @Override
    public void init() throws ServletException {
        userDAO = new UserDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        dispatcher = getServletContext().getRequestDispatcher("/create-new-account");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String password2 = req.getParameter("password2");

        Optional<User> userOptional = userDAO.findByLogin(login);
        if (login=="" || password=="" || password2==""){
            req.setAttribute("error", new String("Incorrect login or password! Try again!"));
            dispatcher = getServletContext().getRequestDispatcher("/create-new-account.jsp");
        } else if (userOptional.isPresent() ){
            req.setAttribute("error", new String("This login alredy exist! "));
            dispatcher = getServletContext().getRequestDispatcher("/create-new-account.jsp");
        } else if (!password.equals(password2)){
            req.setAttribute("error", new String("Passwords are different"));
            dispatcher = getServletContext().getRequestDispatcher("/create-new-account.jsp");
        } else {
            User user = new User(login, password);
            userDAO.create(user);
            req.getSession().setAttribute("userLogged", user);
            dispatcher = getServletContext().getRequestDispatcher("/add-task.jsp"); //zmienić na dalszą stronę
        }

        dispatcher.forward(req, resp);
    }
}



