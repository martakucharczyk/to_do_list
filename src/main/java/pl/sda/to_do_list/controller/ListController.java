package pl.sda.to_do_list.controller;

import pl.sda.to_do_list.model.Task;
import pl.sda.to_do_list.model.TaskDAO;
import pl.sda.to_do_list.model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

@WebServlet(name = "list", value = "/list")
public class ListController extends HttpServlet {
    private TaskDAO taskDAO;
    private RequestDispatcher dispatcher;

    @Override
    public void init() throws ServletException {
        taskDAO = new TaskDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("userLogged");
        long idUserLogged = user.getId();
        Set<Task> tasks = taskDAO.findAllTaskByUserID(idUserLogged);
        req.setAttribute("tasks", tasks);
        if (tasks.size()>0) {
            req.setAttribute("error", new String(""));
        } else {
            req.setAttribute("error", new String("Your TO-DO list is empty"));
        }
        dispatcher = req.getRequestDispatcher("/to-do-list.jsp");
        dispatcher.forward(req, resp);
    }
}
