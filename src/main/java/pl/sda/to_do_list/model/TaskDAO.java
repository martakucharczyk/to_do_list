package pl.sda.to_do_list.model;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

public class TaskDAO {

    private SessionFactory sessionFactory;

    public TaskDAO() {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure()
                .build();
        Metadata metadata = new MetadataSources(registry)
                .buildMetadata();
        sessionFactory = metadata.buildSessionFactory();
    }


    public long create(Task task){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.save(task);
            transaction.commit();
        } catch (RuntimeException e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
        }
        session.close();
        return task.getId();
    }

    public Set<Task> findAllTaskByUserID(Long userId){
        String hql = "FROM Task t WHERE t.user.id = :param";
        Session session = sessionFactory.openSession();
        List<Task> tasksList = session.createQuery(hql, Task.class)
                .setParameter("param", userId)
                .list();
        Set<Task> tasks = new HashSet<>(tasksList);
        session.close();
        return tasks;
    }

    public void deleteTaskById(Long taskId){
        doInTransaction(session -> {
            Task task = session.get(Task.class, taskId);
            if (task != null) {
                session.delete(task);
            }
        });
    }

    public void deleteAllTask(User user) {
        doInTransaction(session -> {
            List<Task> tasks = session.createQuery("FROM Task t WHERE t.user = :param", Task.class)
                    .setParameter("param", user)
                    .list();
            tasks.forEach(session::delete);
        });
    }

    private void doInTransaction(Consumer<Session> action) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            action.accept(session);
            transaction.commit();
        } catch (RuntimeException e) {
            if (transaction.isActive()) {
                transaction.rollback();
            }
        }
        session.close();
    }

}
