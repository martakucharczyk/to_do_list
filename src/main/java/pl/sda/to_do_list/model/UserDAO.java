package pl.sda.to_do_list.model;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;
import javax.persistence.criteria.*;

import java.util.List;
import java.util.Optional;

public class UserDAO {
     private SessionFactory sessionFactory;

    public UserDAO() {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure()
                .build();
        Metadata metadata = new MetadataSources(registry)
                .buildMetadata();
        sessionFactory = metadata.buildSessionFactory();
    }

    public String create(User user){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.save(user);
            transaction.commit();
        } catch (RuntimeException e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
        }
        session.close();
        return user.getLogin();
    }

    public long update(User user) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (session.getTransaction() != null && session.getTransaction().isActive()) {
                session.getTransaction().rollback();
            }
        }
        session.close();
        return user.getId();
    }

    public Optional<User> findByLogin(String login) {
        String hql = "FROM User u WHERE u.login=:param";
        Session session = sessionFactory.openSession();
        Optional<User> user = session.createQuery(hql, User.class)
                .setParameter("param", login)
                .uniqueResultOptional();
        session.close();
        return user;
    }

    public Optional<User> findById(long id) {
        String hql = "FROM User u WHERE u.id=:param";
        Session session = sessionFactory.openSession();
        Optional<User> user = session.createQuery(hql, User.class)
                .setParameter("param", id)
                .uniqueResultOptional();
        session.close();
        return user;
    }





}
