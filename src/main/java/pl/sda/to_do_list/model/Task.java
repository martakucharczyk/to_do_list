package pl.sda.to_do_list.model;

import lombok.*;
import javax.persistence.*;
import java.time.LocalDateTime;


@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "task")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    private String title;

    @Column
    private String content;

   /* @Column
    private LocalDateTime modificationDateTime;*/

    @ManyToOne
    private User user;

    public Task(String title, String content, User user) {
        this.title = title;
        this.content = content;
        this.user = user;
       /* this.modificationDateTime = LocalDateTime.now();*/
    }


    @Override
    public String toString() {
        return ("Id: " + id + "  Title:  " + title + "\n" + "Content:  " + content);
    }

}
